<!doctype html>
<html lang="en">

  <head>
    <title>Vladan Miskovic</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,700|Oswald:400,700" rel="stylesheet">

    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Jost&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/aos.css">
    <link  rel="icon" href="images/logo.png" type="image/png" />

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="css/style.css">

  </head>

  <!-- <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300"> -->
    <body >

    <div id="overlayer"></div>
    <div class="loader" style="color:black !important;">
      <div style="color:black !important;" class="spinner-border text-primary" role="status">
        <span class="sr-only">Loading...</span>
      </div>
    </div>

    <div class="site-wrap" id="home-section">

      <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
          <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
          </div>
        </div>
        <div class="site-mobile-menu-body"></div>
      </div>


      <header class="site-navbar js-sticky-header site-navbar-target" role="banner">

        <div class="container">
          <div class="row align-items-center position-relative">


            <div class="site-logo">
              <a href="index.php" class="text-black"><span style="color:black !imporatant; font-family: 'Josefin Sans', sans-serif;">Vladan Miskovic</a>
            </div>

            <div class="col-12">
              <nav class="site-navigation text-right ml-auto " role="navigation">

                <ul class="site-menu main-menu js-clone-nav ml-auto d-none d-lg-block">
                  <li><a href="index.php" class="nav-link link-nav">Home</a></li>


                  <li class="has-children">
                    <a href="#about-section" class="nav-link link-nav">Accomplishments</a>
                    <ul class="dropdown arrow-top">
                      <!-- <li class="has-children">
                        <a href="#">BET</a>
                        <ul class="dropdown">
                          <li><a href="#">Sandler approach to Sales</a></li>
                          <li><a href="#">World class cost/price modelling</a></li>
                        </ul>
                      </li>
                      <li class="has-children">
                        <a href="#">Sales Strategy</a>
                        <ul class="dropdown">
                          <li><a href="#">New Business Win Strategy</a></li>
                          <li><a href="#">World class cost/price modelling</a></li>
                        </ul>
                      </li> -->
                      <li >
                        <a href="bet.html">BET</a>

                      </li>
                      <li >
                        <a href="sales.html">Sales Strategy</a>

                      </li>
                      <li >
                        <a href="product.html">Product Management</a>

                      </li>

                    </ul>
                  </li>

                  <li>
                    <a href="valueproposition.html" class="nav-link link-nav">Value Proposition</a>
                  </li>
                  <li><a href="resume.html" class="nav-link link-nav">Resume</a></li>
                  <li><a href="#contact-section" class="nav-link link-nav">Contact</a></li>
                  <!-- <li class="has-children">
                    <a href="#testimonials-section" class="nav-link link-nav">Value Proposition</a>
                    <ul class="dropdown arrow-top">
                      <li><a href="#team-section" class="nav-link link-nav">Business Model for Growth</a></li>
                      <li><a href="#pricing-section" class="nav-link link-nav">Strategic Policy Deployment (SDP)</a></li>
                    </ul>
                    <li><a href="resume.php" class="nav-link link-nav">Resume</a></li>
                    <li><a href="#contact-section" class="nav-link link-nav">Contact</a></li>
                  </li> -->
                </ul>
              </nav>

            </div>

            <div class="toggle-button d-inline-block d-lg-none"><a href="#" class="site-menu-toggle py-5 js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

          </div>
        </div>

      </header>
      <hr>
      <div class="ftco-blocks-cover-1" style="margin-top: -41px; padding-left: 50px; padding-right: 50px; padding-top: 50px;">
        <br>
        <div class="container-for-image" style="border-radius: 300px 20px 5px;">
          <div class="over-image" style="border-radius: 300px 20px 5px;"><h1 style="text-align:center; font-size:61px !important;">VLADAN MISKOVIC</h1>
            <h3 style="text-align:center;">Corporate Executive</h3>
            <div style="text-align:center;">
              <a style="color:white; width: 100%; left: 0; bottom: 10px; position: absolute;" href="#contact-section" class="btn btn-primary" type="button" name="button">REQUEST APPOINTMENT </a>
            </div>
          </div>

          <div class="color-overlay" style="border-radius: 300px 20px 5px;"></div>
        </div>


      </div>
      <br><br>
      <div class="video-section">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <figure class="block-img-video-1" data-aos="fade">
                    <!-- <a href="https://vimeo.com/user115264664/review/417397432/9779277e5d" target="_blank" data-fancybox data-ratio="2"> -->
                      <a href="https://vimeo.com/417397432" target="_blank">
                    <span class="icon"><span class="icon-play"></span></span>
                    <img src="images/vlad.jpg" alt="Image" class="img-fluid">
                  </a>
                  </figure>
            </div>
            <div class="col-md-6">
              <div class="vl" style="border-left: 6px solid #d97600;
  height: 100px;"><h1 style="padding:16px !important; color:#333; ">Leadership Profile</h1></div><br>
  <p style="text-align:justify;">Corporate Executive with proven industry leadership in the field of P&amp;L management,
Operations, Global Sales and Marketing, Strategy Planning and Deployment, Multi Company
integration/optimization, and M&amp;A Execution. With a strong financial acumen, drove successful
results in top and bottom line growth in leading contract manufacturing firms within Aerospace,
Defense and Medical Device markets. Successfully built Teams and Processes to drive winning
culture in an organization. A corporate professional with 23 years of Aerospace and Defense, 4
years of Medical devices, 15 years of Private Equity, and 8 years of BOD experience.</p>

            </div>

          </div>
        </div>
      </div>
      <div class="blog-section">
        <br><br>

        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="vl" style="border-left: 6px solid #d97600;
  height: 100px;"><h1 style="padding:10px !important; color:#333; ">Blog</h1> <h5 style="padding:16px !important;">Employees come first</h5> </div>
            </div>
            <div class="col-md-6">

            </div>
          </div>
        </div>
        <br><br>
        <div class="container">
          <div class="row">
            <div class="col-md-2">

            </div>
            <div class="col-md-8">
              <div class="card">
                <img src="images/team.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                  <!-- <h5 class="card-title">Card title</h5> -->
                  <p class="card-text" style="text-align:justify;">Under the guidance of world’s best Leadership coaches, Simon Sinek and John Maxwell, I have
lived by this principle since my first people leadership job in 2000.  This is exactly how good
leaders become great leaders and has been a fundamental principle of my success. I have long
defined myself as a People leader and the “People come first” leadership philosophy has been a
fundamental principle of my success.</p>
                  <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                </div>
              </div>
            </div>

            <div class="col-md-2">

            </div>
          </div>
        </div>
      </div>

      <div class="blog-section">
        <br><br>

        <div class="container">
          <div class="row">
            <div class="col-md-9">
              <div class="vl" style="border-left: 6px solid #d97600;
  height: 100px;"><h1 style="padding:10px !important; color:#333; ">Accomplishments</h1> <h5 style="padding:16px !important;">A selection of problems and challenges I have been instrumental in solving.</h5> </div>
            </div>
            <div class="col-md-3">

            </div>
          </div>
        </div>
        <br><br>
        <div class="container">
          <div class="row">

          </div>
        </div>
      </div>

      <div class="site-section bg-light" id="services-section">
        <div class="container">
          <div class="row">
            <div class="col-md-4">
              <div class="block__35630" style="height: 735px;">
                <div class="icon mb-0">
                  <img src="images/idea.png" style="height:70px;" alt="">
                </div>
                <h3 class="mb-3">BET (Business Engagement Team)</h3>
                <p style="text-align:justify;">Over the last 10 years contract manufacturers have been under
significant pricing pressures. In order to maintain the profitability and growth my
team had to attain sophistication to proposal development process and strategy.</p>
<a href="bet.html" type="button" class="btn btn-primary" style="color: white;" name="button">READ</a>
              </div>
            </div>
            <div class="col-md-4">
              <div class="block__35630" style="height: 735px;">
                <div class="icon mb-0">
                  <img src="images/bussines.png" style="height:70px;" alt="">
                </div>
                <h3 class="mb-3">Sales Strategy</h3>
                <p style="text-align:justify;">All large OEMs and Tier 1 Suppliers are looking for strategic partners
in the supply chain. They are looking for commitment, performance and value
creation solutions. In order to succeed, my team and I have created a strategy that
is focused on key programs, products and processes thru value and velocity. Or
mathematically expressed, Strategy = 3P + V 2</p>
<a href="sales.html" type="button" class="btn btn-primary" style="color: white;" name="button">READ</a>
              </div>
            </div>
            <div class="col-md-4">
              <div class="block__35630" style="height: 735px;">
                <div class="icon mb-0">
                  <img src="images/bombilla.png" style="height:70px;" alt="">
                </div>
                <h3 class="mb-3">Product Management</h3>
                <p style="text-align:justify;">As companies grow and customers and products it becomes
exponentially more important to understand how each product group is
performing financially, is it aligned with growth plans and is it mutually strategic
to customer and the company.</p>
<a href="product.html" type="button" class="btn btn-primary" style="color: white;" name="button">READ</a>
              </div>
            </div>
          </div>


        </div>
      </div>







    <div class="site-section bg-light" id="contact-section">
      <div class="container">
        <div class="row">
          <div class="col-12 text-center mb-5" data-aos="fade-up" data-aos-delay="">
            <div class="block-heading-1">
              <span>Get In Touch</span>
              <h2>Contact Me</h2>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 mb-5" data-aos="fade-up" data-aos-delay="100">
            <form action="#" method="post">
              <div class="form-group row">
                <div class="col-md-6 mb-4 mb-lg-0">
                  <input id="name" type="text" class="form-control" placeholder="First name">
                </div>
                <div class="col-md-6">
                  <input id="lastname" type="text" class="form-control" placeholder="Last name">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-12">
                  <input id="email" type="text" class="form-control" placeholder="Email address">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-12">
                  <textarea name="" id="text" class="form-control" placeholder="Write your message." cols="30" rows="10"></textarea>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-md-6 mr-auto">
                  <button style="color:white;" onclick="sendEmail();" class="btn btn-primary" type="button" name="button">Send</button>
                </div>
              </div>
            </form>
          </div>
          <div class="col-lg-4 ml-auto" data-aos="fade-up" data-aos-delay="200">
            <div class="bg-white p-3 p-md-5">
              <h3 class="text-black mb-4">Contact Info</h3>
              <ul class="list-unstyled footer-link">
                <li class="d-block mb-3">
                  <span class="d-block text-black">Address:</span>
                  <span>182 Via Veracruz Jupiter FL 33458</span>
                </li>
                <li class="d-block mb-3"><span class="d-block text-black">Phone:</span><span>+1 (561) 635-3870</span></li>
                <li class="d-block mb-3"><span class="d-block text-black">Email:</span><span>Vladan.Miskovic@my.trident.edu</span></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>


    <footer class="site-footer">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-7">
                <h2 class="footer-heading mb-4">VLADAN MISKOVIC</h2>
                <p>Corporate Executive. </p>
              </div>
              <div class="col-md-4 ml-auto">
                <h2 class="footer-heading mb-4">Features</h2>
                <ul class="list-unstyled">
                  <li><a href="index.php">Home</a></li>
                  <li><a href="bet.html">BET</a></li>
                  <li><a href="sales.html">Sales Strategy</a></li>
                  <li><a href="product.html">Product Managment</a></li>
                  <li><a href="contact.html">Contact Us</a></li>
                </ul>
              </div>

            </div>
          </div>
          <!-- <div class="col-md-4 ml-auto">

            <div class="mb-5">
              <h2 class="footer-heading mb-4">Subscribe to Newsletter</h2>
              <form action="#" method="post" class="footer-suscribe-form">
                <div class="input-group mb-3">
                  <input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-primary text-white" type="button" id="button-addon2">Subscribe</button>
                  </div>
                </div>
            </div>


            <h2 class="footer-heading mb-4">Follow Us</h2>
            <a href="#about-section" class="smoothscroll pl-0 pr-3"><span class="icon-facebook"></span></a>
            <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
            <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
            <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
            </form>
          </div> -->
        </div>

      </div>
    </footer>

    </div>

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/jquery.sticky.js"></script>
    <script src="js/jquery.waypoints.min.js"></script>
    <script src="js/jquery.animateNumber.min.js"></script>
    <script src="js/jquery.fancybox.min.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/aos.js"></script>

    <script src="js/main.js"></script>

    <script type="text/javascript">
      $(document).ready(function () {})
      function sendEmail() {
        var name = $('#name').val(),
            lastname = $('#lastname').val(),
            email = $('#email').val(),
            text = $('#text').val();
        if (name == '') {
          alert('Name is empty')
        }else if(lastname == ''){
          alert('Last Name is empty')
        }else if(email == ''){
          alert('email is empty')
        }else if(text == ''){
          alert('Text is empty')
        }else{
          // alert('else')
          $.ajax({
            method:'POST',
            url:'ajax/sendEmail.php',
            data:{name:name, lastname:lastname, email:email, text:text},
            success:function (res) {
              console.log(res);
            }
          })
        }
      }
    </script>
  </body>

</html>
